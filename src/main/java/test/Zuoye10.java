package test;

public class Zuoye10 {
    /**
     * 执行程序，分析并解决问题
     *
     * NumberFormatException出现的情况通常是由包装类将字符串解析为基本类型时,
     * 由于字符串内容不能正确描述基本类型导致该异常.
     * 数字    格式      异常
     *
     *
     * @author Xiloer
     *
     */

        public static void main(String[] args) {
            /*
             * 原因:出现非法字符，空格
             */
            String num = "123";
            int d = Integer.parseInt(num);
            System.out.println(d);

            /*
             * 原因:类型不一样
             */
            //num="123.123";
            num = "123";
            d = Integer.parseInt(num);
            System.out.println(num);

            /*
             * 原因:分号应是英文的
             *
             */
		num = "123";
		d = Integer.parseInt(num);
		System.out.println(d);
        }
    }

