package test;

import java.util.Scanner;

/**
 * 要求用户在控制台输入自己的用户名。
 * 然后要求做如下验证工作:
 * 1:用户名不能为空(只要有一个字符)
 * 2:用户名必须在20个字以内
 * @author Xiloer
 *
 */

public class Zuoye2 {
    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
        System.out.println("输入用户名");
        String a=scan.nextLine();
        a=a.trim();
       int  b=a.length();
       if(b>0&&b<=20){
           System.out.println("验证通过");
       } else {
           System.out.println("不合法");
       }

    }
}
