package test;

import java.io.*;

/**
 * 改正下面程序的错误
 *
 * 向文件testpw.txt中以UTF-8编码写入一行字符串：
 * 你好!我喜欢java!
 *
 * 单词记一记:
 * print 打印
 *
 * @author Xiloer
 *
 */
public class Zuoye17 {
    //要有main方法
    public static void main(String[] args) {
        try (
                FileOutputStream fos = new FileOutputStream("testpw.txt");
            /*
                java.io.UnsupportedEncodingException: UFT-8
                说明指定的字符集名字错误
             */
                OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
                BufferedWriter bw = new BufferedWriter(osw);
                PrintWriter pw = new PrintWriter(bw);
        ) {
            pw.println("你好!我喜欢java!");
            //System的S没有大写
//            system.out.println("写出完毕!");
            System.out.println("写出完毕!");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("出错了!");
        }
    }
}
