package test;
import java.io.File;
import java.io.FileFilter;
/**
 * 列出当前目录中所有名字包含s的子项。
 *
 * 使用匿名内部类和lambda两种写法
 *
 * 单词记一记:
 * FileFilter   文件过滤器
 * accept       接受
 *
 * @author Xiloer
 *
 */

public class ZuoYe13 {
    public static void main(String[] args) {
        File su = new File(".");
        if (su.isDirectory()) {
//			File[] subs = su.listFiles(new FileFilter() {
//				public bool ean accept(File file) {
//					return file.getName().contains("s");
//				}
//			});

        }
        File[] subs = su.listFiles((file) -> file.getName().contains("s"));
        for (int i = 0; i < subs.length; i++) {
            System.out.println(subs[i].getName());


        }
    }
}
