package test;

import java.io.*;

/**
 * 扫描指定目录中的所有.java文件，并将内容全部输出到控制台
 *
 * 例如将当前项目目录下src/io目录中的所有java文件内容输出
 * 到控制台
 *
 * 1:先要定位./src/io目录(哪个API用来描述目录?)
 * 2:获取该目录下的所有.java文件
 * 3:遍历每一个java文件，然后按行读取里面的每一行字符串
 *   并输出控制台
 *
 *   注意，流用完了要关闭，可以用autocloseable特性
 *
 *
 * @author Xiloer
 *
 */
public class Zuoye20 {
    public static void main(String[] args) {
        File dir = new File("./src/main/java/io");
        //获取当前目录下的所有.java文件
        if (dir.isDirectory()) {
            File[] subs = dir.listFiles((f -> f.getName().endsWith(".java")));
            for (int i = 0; i < subs.length; i++) {
                File sub = subs[i];
                try (
                        FileInputStream fis = new FileInputStream(sub);
                        InputStreamReader isr = new InputStreamReader(fis);
                        BufferedReader br = new BufferedReader(isr);
                ) {
                    String line;
                    while((line = br.readLine())!=null){
                        System.out.println(line);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}