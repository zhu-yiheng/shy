package test;

/**
 * 修改下面代码的错误
 * <p>
 * 下面代码完成的功能是输出字符串中的每一个字符
 *
 * @author Xiloer
 */
public class Zuoye3 {
    public static void main(String[] args) {
        //main方法
        //String字母大写
        String str = "hello world! i love java!";
        //字符串的length是方法!
        //i应当是小于length
        for (int i = 0; i < str.length(); i++) {

            //charAt是String的方法
            // char c = i.charAt(i);
            char c = str.charAt(i);
            //输出信息应当用System.out进行，err是用来输出错误信息的
            // System.err.println(c);
            System.out.println(c);
        }

    }
}
