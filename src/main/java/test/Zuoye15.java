package test;

import java.io.*;

/**
 * 改正下面程序的错误
 * <p>
 * 程序实现需求:使用缓冲流完成文件的复制操作
 *
 * @author Xiloer
 */
public class Zuoye15 {
    public static void main(String[] args) throws IOException {


        FileInputStream fis = new FileInputStream("test.txt");
        BufferedInputStream bis = new BufferedInputStream(fis);

        FileOutputStream fos = new FileOutputStream("test_cp.txt");
        BufferedOutputStream bos = new BufferedOutputStream(fos);

        int d;
        while ((d = bis.read()) != -1) {//要判断不等于-1才是没有到文件末尾，循环要使用while

            bos.write(d);
        }
        System.out.println("复制完毕!");
        bis.close();
        bos.close();

    }

}
