package test;

import jdk.nashorn.internal.ir.LexicalContext;
import jdk.nashorn.internal.ir.LexicalContextNode;

import java.io.*;
import java.util.Scanner;

/**
 * 编写一个记事本工具
 *
 * 程序启动后，要求用户输入一个文件名。如果该文件不存在
 * 则直接创建。
 * 如果存在，则提示用户:
 * "该文件已存在，是否更换文件名?[y]更换，[n]不更换"
 * 让用户输入y或n决定
 * 如果更换则重新只要求用户输入文件名。
 *
 * 如果不更换，则提示用户:
 * 是重新编写文件内容，还是继续写入文件内容?[y]重新 [n]继续
 * 如果是重新写，则将该文件原来的数据全部抹除
 * 如果是继续写，则将写入的内容都追加到文件中。
 *
 * 之后用户输入的每一行字符串都要写入到文件中，当用户单独
 * 输入"exit"是程序退出。
 *
 *
 *
 * @author Xiloer
 *
 */

public class Zuoye19 {




    public static void main(String[] args) throws FileNotFoundException {
        Scanner scan=new Scanner(System.in);
        File file=null;
        boolean append = false;//不追加写操作
        while(true){
            System.out.println("请输入文件名");
            String fileName=scan.nextLine();
            file=new File(fileName);
            if(file.exists()){
                //如果文件存在，提示该文件已存在
                System.out.println("该文件已存在，是否更换名字?[y]更换 [输入其他内容]不更换");
                String input = scan.nextLine();
                if("y".equalsIgnoreCase(input)){
                    continue;
                } else{
                    //不更换名字，询问是否追加内容?
                    System.out.println("是重新编写文件内容，还是继续写入文件内容?[y]重新 [n]继续");
                    input = scan.nextLine();
                    if("n".equalsIgnoreCase(input)){//N表示追加写
                        append = true;
                    }
                    break;
                }
            }else {//如果文件不存在，就不用让用户再重新输入了
                break;
            }
        }
        try(
                PrintWriter pw = new PrintWriter(
                        new BufferedWriter(
                                new OutputStreamWriter(
                                        new FileOutputStream(file,append)
                                )
                        )
                );
        ){
            System.out.println("请开始输入内容，单独输入exit退出");
            while(true){
                String line = scan.nextLine();
                if("exit".equalsIgnoreCase(line)){
                    break;
                }
                pw.println(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}