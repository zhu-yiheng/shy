package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * 将当前目录下的所有obj文件获取到，并进行
 * 反序列化后输出每个用户的信息(直接输出反序
 * 列化后的User对象即可)
 * @author Xiloer
 *
 */

public class Zuoye23 {
    public static void main(String[] args) {
        File dir = new File(".");
        if (dir.isDirectory()) {
            File[] subs = dir.listFiles((f) -> f.getName().endsWith(".obj"));
            for (int i = 0; i < subs.length; i++) {
                try (
                        FileInputStream fis = new FileInputStream(subs[i]);
                        ObjectInputStream ois = new ObjectInputStream(fis);
                ) {
                    Object obj = ois.readObject();
                    if(obj instanceof User){
                        User user = (User)obj;
                        System.out.println(user);
                    }


                } catch (ClassNotFoundException | IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

