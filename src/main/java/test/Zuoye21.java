package test;

import java.io.*;
import java.util.Scanner;

/**
 * 改错
 *
 * 程序实现的是简易记事本工具。程序启动后向pw.txt文件写内容
 * 用户输入的每一行字符串都写入到文件中，单独输入exit时
 * 程序退出。
 *
 * @author Xiloer
 *
 */
public class Zuoye21 {
	public static void main(String[] args) {
		try (
				//写操作要用输出流
//		FileInputStream fos = new FileInputStream("pw.txt");
				FileOutputStream fos = new FileOutputStream("pw.txt");
				//字符集的名字拼写错误!
//                OutputStreamWriter osw = new OutputStreamWriter(fos, "UFT-8");
				OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
				BufferedWriter bw = new BufferedWriter(osw);
				PrintWriter pw = new PrintWriter(bw, true);
		) {


			Scanner scanner = new Scanner(System.in);
			System.out.println("请开始输入内容");
			while (true) {
				String str = scanner.nextLine();
				if ("exit".equals(str)) {
					break;
				}
//                pw.println(srt);//变量名拼写错误
				pw.println(str);
//                pw.close();//流的关闭不能写在循环里面
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

