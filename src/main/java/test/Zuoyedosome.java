package test;



/**
 * 修改下面的代码,使得两个线程执行dosome方法时是同步的
 * @author Xiloer
 *
 */
public class Zuoyedosome {
    public static void main(String[] args) {


        Thread t1 = new Thread() {
            public void run() {
                Boo.dosome();
            }
        };
        Thread t2 = new Thread() {
            public void run() {
                Boo.dosome();
            }
        };
        t1.start();
        t2.start();
    }
}
class Boo{
    public static void dosome() {
        Thread t = Thread.currentThread();
        synchronized (Boo.class) {
            try {
                System.out.println(t.getName()+":正在执行dosome方法...");
                Thread.sleep(5000);
                System.out.println(t.getName()+":执行dosome方法完毕!");
            } catch (Exception e) {
            }
        }
    }
}

