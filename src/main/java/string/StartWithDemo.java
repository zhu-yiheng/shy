package string;

/**
 * boolean startsWith(String str)
 * boolean endsWith(String str)
 * 判断当前字符是否是已给定的字符串开始或结束的。
 */
public class StartWithDemo {
    public static void main(String[] args) {
        String line="http://www.tedu.com";

       boolean starts=line.startsWith("http");//true
       System.out.println("starts:"+starts);
       boolean ends=line.endsWith(".com");
       System.out.println("ends:"+ends);//true
        boolean s=line.startsWith(":");
        System.out.println(":"+s);
        String line1 ="http://www.baidu.com";
       boolean starts1=line1.startsWith("http");
        System.out.println("starts1:"+starts1);
        boolean s1=line1.endsWith(".com");
    }
}
