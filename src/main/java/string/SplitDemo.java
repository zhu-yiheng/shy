package string;

import java.sql.SQLOutput;
import java.util.Arrays;

/**
 * String[] split(String regex)
 * 将当前字符串中正则表达式的部分进行拆分，将拆分的每段内容以数组形式返回
 */

public class SplitDemo {
    public static void main(String[] args) {

        String str="abc123def456ghi";
        //按照数字部分进行拆分，将所有字符串部分得到
        String[] data=str.split("[0-9]+");
        System.out.println(data.length);
        System.out.println(Arrays.toString(data));
        //拆出字母部分
         str="abc,def,ghi,jkl";
        data=str.split(","+"");
        System.out.println(Arrays.toString(data));

       str="abc.def.ghi.jkl";
        data=str.split("\\.");//因为“.”表示任意字符，所以不能直接拆，需要加“\\"
        System.out.println(Arrays.toString(data));
        //当连续遇到两个拆分项时，中间会多拆出一个空字符串
        //如果字符串一开始就是拆分项，则会先拆分出一个
        //如果字在符串末尾连续出现拆分项，则所有字符串的空字符串都会舍弃
        str="abc..def.ghi.jkl";
        data=str.split("\\.");
        System.out.println(Arrays.toString(data));

        str=".abc..def.ghi.jkl";
        data=str.split("\\.");
        System.out.println(Arrays.toString(data));

        str=".abc..def.ghi.jkl...............g";
        data=str.split("\\.");
        System.out.println(Arrays.toString(data));

    }
}
