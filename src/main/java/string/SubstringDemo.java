package string;

/**
 * String substring(int start,int end)
 * 截取当前字符串中指定范围内的字符串。两个参数分别为开始位置的下标和结束位置的下标。
 * 注:在JAVA API中通常使用两个数字表示范围时是"含头不含尾"的。
 */
public class SubstringDemo {
    public static void main(String[] args) {
        //             01234567890
        String line = "www.tedu.cn";
        //截取域名tedu
        String str = line.substring(4,8);
        System.out.println(str);

        //重载的方法是从指定位置开始截取到字符串末尾
        str = line.substring(4);
        System.out.println(str);
        String line1="www.baidu.com";
        String a=line1.substring(4,9);
        System.out.println(a);
        a=line1.substring(9);
        System.out.println(a);//.com

    }
}