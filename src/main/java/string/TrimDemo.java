package string;

import javax.management.ListenerNotFoundException;

/**
 * String trim()
 * 去除一个字符串两边的空白字符
 */
public class TrimDemo {
    public static void main(String[] args) {
        String line="  hello        ";
        System.out.println(line);

        String trim=line.trim();//调trim方法
        System.out.println(trim);

        String a1="    a        ";
        String b1=a1.trim();
        System.out.println(b1);

    }
}
