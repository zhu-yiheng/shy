package string;

/**
 * String toUperCase()
 * String  toLowerCase()
 *  String  变量=字符串的变量.toLowerCase()/toUperCase()
 *  再输出变量得到的是全小写或全大写；
 * 将当前字符串中的英文部分转换为全大写或全小写
 */
public class ToUperCaseDemo {
    public static void main(String[] args) {
        String line="我爱Java";
        String a=line.toLowerCase();
       System.out.println(a);
       String b=line.toUpperCase();
       System.out.println(b);

       String c="adac";
       String d=c.toUpperCase();
       System.out.println(d);
    }
}
