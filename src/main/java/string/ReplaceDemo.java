package string;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

/**
 * 字符串支持的正则表达式三
 * String replaceAll(String regex,String str)
 * 将当前字符串中满足正则表达式的部分替换为给定内容
 */

public class ReplaceDemo {
    public static void main(String[] args) {
        String str="abc123def456ghi";
        //将字符串的数字换成#NUMBER#
        str=str.replaceAll("[0-9]+","#NUMBER#");
        System.out.println(str);

        str=str.replaceAll("[a-z]+","#NUMBER#");
        System.out.println(str);
        //和谐用语
        String regex="(cnm|dsb)";
        String message="cnm.!你个dsb，你怎么dsb)";
        message =message.replaceAll(regex,"**");
        System.out.println(message);
    }
}
