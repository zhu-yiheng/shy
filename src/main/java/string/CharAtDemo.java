package string;

/**
 * char charAt(in index)
 * 返回当前字符串指定位置的字符
 */
public class CharAtDemo {
    public static void main(String[] args) {
        //          0123456789012345
        String str="thinking in java";
        char c=str.charAt(9);
        System.out.println(c);//i
        char b=str.charAt(12);//j
        System.out.println(b);
        //判断回文 正着反着念是一句话
        String line="上海自来水来自海上";

        for(int i=0;i<line.length()/2;i++){
            System.out.println(i);
        }

    }
}
