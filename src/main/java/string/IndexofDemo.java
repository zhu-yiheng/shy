package string;

/**
 * int indexof(String str)
 * 检索给定字符在当前字符串中的位置，若当前字符串不含有给定内容，则返回值为-1
 */

public class IndexofDemo {
    public static void main(String[] args) {
        //          0123456789012345
        String str = "thinking in java";
        int index = str.indexOf("ink");//字符当前的位置
        System.out.println(index);//2,返回的是第一个字母的位置

        //重载的方法可以从指定的位置开始，检索第一次出现给定字符串的位置
        index = str.indexOf("in", 7);
        System.out.println(index);//5
        //检测最后一次出现in的位置
        index = str.lastIndexOf("in");
        System.out.println(index);//9

        String z = "wo love Demo";
        int a = z.indexOf("ove");
        System.out.println(a);//4
        a = z.indexOf("o", 3);
        System.out.println(a);//4.
    }
}
