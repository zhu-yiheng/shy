package string;

import java.sql.SQLOutput;

/**
 * 字符串常量池的优化导致字符串是不变对象，这只考虑了重用性，但是副作用是内次修改内容都要创建新对象，因此字符串不适合频繁修改，性能低下
 */
public class StringDemo2 {
    public static void main(String[] args) {
        String str="a";
        for(int i=0;i<10000000;i++){
            str+=str;
        }
        System.out.println("执行完毕");
    }
}
