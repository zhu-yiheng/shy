package string;

/*
返回当前字符的长度
 */
public class LengthDemo {
    public static void main(String[] args) {
        String str = "我爱Java!";
        int len = str.length();
        System.out.println("len：" + len);//7

        String str1 = "nihao";
        int len1 = str1.length();
        System.out.println(len1);
        String n1 = "shauixdasnc";
        int a = n1.length();
        String n2 = "saico";
        int c = n2.length();
        System.out.println(c);//5
        String a1="cxnsaiocj";
        int m=a1.length();
        System.out.println(m);
    }
}
