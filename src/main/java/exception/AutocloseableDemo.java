package exception;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * JDK7之后推出了一个新的特性：自动关闭性
 * 这是在异常处理中使用的特性，可以让编译器自动添加关闭的操作
 * 语法：
 * try(这里声明和初始化需要在finally中调用close关闭的变量){
 *     代码片段
 * } catch(XXXException e){}
 * 上述代码在try后面（）中声明的内容最终会被编译器改为finally中close关闭
 */
public class AutocloseableDemo {
    public static void main(String[] args) {
        //做finallydemo2同样的工作
        try(
                FileOutputStream fos=new FileOutputStream("fos.dat");
                //只有实现了AtoClosedabale借楼的类才可以在这里声明！
                //String str=“！！！！！”；

        ){
            fos.write(1);
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
