package exception;

/**
 * throw关键字，用于主动对外抛出异常
 * crt+alt+t调使用的条件，先选中
 */
public class ThrowDemo {
    public static void main(String[] args) {
        System.out.println("程序开始了...");
        try {
            Person p=new Person();
            /*
            当我们调用一个含有throws声明异常抛出的方法时，编译器要求
            我们必须添加异常处理的手段，否则编译不通过，而处理手段欧；两种
            1：使用try-catch捕捉异常处理
            2：在当前方法继续使用throws声明该的异常抛出
            具体使用哪种取决于异常处理的责任问题
             */
            p.setAge(100000);//典型的符合语法，但不符合业务逻辑要求
            System.out.println("此人的年龄"+p.getAge()+"岁");
        } catch (IllegalAgeException e) {
            e.printStackTrace();
        }
        System.out.println("程序结束了");
    }
}
