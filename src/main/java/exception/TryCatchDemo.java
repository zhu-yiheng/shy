package exception;

/**
 * 异常处理机制中的try-catch
 * 语法：
 * try{
 *     代码片段
 * } catch(xxxException){
 *     d当try代码中出现xxxException后的解决办法
 * }
 */

public class TryCatchDemo {
    public static void main(String[] args) {
        System.out.println("程序开始了");
        try{
            //String str = null;
          //  String str="";
            String str="a";
        /*
        当Str为null时，虚拟机执行下面代码，发现出现了空指针，于是会实例化一个
        NullPointerException,并将报错的位置设置进去，然后将异常抛出
         */
            System.out.println(str.length());
            System.out.println(str.charAt(0));
            System.out.println(Integer.parseInt(str));
            //try语句块，报错代码以下的内容不会被执行
            System.out.println("!!!!!!!!!!!!!!");
    } catch(NullPointerException e){
            System.out.println("出现了控制针");
            //catch可以定义多个，当针对不同异常有不同操作时要分别捕获处理
    } catch(StringIndexOutOfBoundsException e){
            System.out.println("出现了字符串下标越界");
            /*
            我们可以捕获超类的异常，通常情况会这样做：
            1：
            当try中出现了积累不同的异常，但是解决办法相同时，可以捕获这些类异常的统一超类，进行统一处理
            2：在最后一个catch处捕获异常Exception可以尽量避免因为一个未知异常导致程序中断
             */
        }catch(Exception e){
            System.out.println("反正就是出了个错！");
        }
        System.out.println("程序结束");
    }

}
