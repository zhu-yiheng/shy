package exception;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * IO操作中异常处理机制
 */
public class FinallyDemo2 {
    public static void main(String[] args) throws FileNotFoundException {
        FileOutputStream fos =null;
        try {
           fos = new FileOutputStream("fos.txt");
            fos.write(1);
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();//将报错信息输出到控制台
        } finally {
            try {
                if(fos!=null){
                    fos.close();
                }
            }catch (IOException e) {
                e.printStackTrace();//将报错信息输出到控制台
            }
        }
    }
}
