package exception;

/**
 * finally块
 * finally块是异常处理机制的最后一块，它可以直接跟在try语句块之后
 * 或者最后一个catch之后
 *
 *
 * finally块可以保证只要程序进行到try语句块，无论try语句块的代码是否出现异常，
 * finally块中的代码都必定执行
 *
 * 通常我们会将释放资源这类操作放在finally中确保执行。比如流的关闭操作
 */

public class FinallyDemo {
    public static void main(String[] args) {
        System.out.println("程序开始了..");
        try{
            String str="null";
            System.out.println(str.length());
            return;//就算有return，也要等finally执行后方法才会真正返回
        } catch (Exception e){
            System.out.println("出错了");
        }finally{
            System.out.println("finall中的代码执行了");
        }
        System.out.println("程序结束...");
    }
}
