package exception;

/**
 * 测试异常的抛出
 */
public class Person {
    private int age;

    public int getAge() {
        return age;
    }
    /*
    当一个方法使用throws声明异常抛出时，调用此方法的代码片段就要必须处理这个异常
     */

    public void setAge(int age) throws IllegalAgeException{
        if(age<0|| age>100){
            //使用throw抛出一个异常
            //除了RuntimeException之外，抛出什么异常就要在方法上声明throws什么异常
          //  throw new Exception("年龄不合法");
            //抛出自定义异常
            throw new IllegalAgeException("年龄超范围"+age);
        }
        this.age = age;
    }
}
