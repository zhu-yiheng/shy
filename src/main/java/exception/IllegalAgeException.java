package exception;

/**
 * 非法的年龄异常
 *
 * 自定义异常通常来说明业务上的错误
 * 自定义异常要注意以下问题
 * 1：定义的类名要做到见名知意
 * 2：必须是Exception的子类
 * 3：提供Exception所定义的所有构造方法alt+insert+constr
 */
public class IllegalAgeException extends Exception{
    public IllegalAgeException() {
    }

    public IllegalAgeException(String message) {
        super(message);
    }

    public IllegalAgeException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalAgeException(Throwable cause) {
        super(cause);
    }

    public IllegalAgeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
