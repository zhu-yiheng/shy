package exception;

/**
 * 和异常相关的面试题
 * 请分别说出final，finally，finalize
 * finalize是Object中定义的一个方法，他是提对象生命周期中最后一个被调用的方法
 * 当一个对象即将被GC回收前，GC会调用该对象的finalize方法，该方法调用完毕后
 * 就会被释放，Api手册对该方法有明确说明，
 * 此方法不能有耗时的操作，否则就会影响GC的工作
 */
public class FinallyDemo3 {
    public static void main(String[] args) {
        System.out.println(
                test("1")+","+test(null)+","+test("")
        );//3,3,3
    }
    public static  int test(String str){
       try{
           return str.charAt(0);
       } catch (NullPointerException e){
           return 1;
       } catch (Exception e){
           return 2;
       } finally {
           return 3;
       }
    }
}
