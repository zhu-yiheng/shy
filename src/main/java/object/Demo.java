package object;

/**
 * 测试object中的相关方法
 * object是所有顶级类的超类，里面常被子类重写的方法：
 * equals()和toString()
 */
public class Demo {
    public static void main(String[] args) {
        Point p=new Point(1,2);
        /*
        Object定义的方法：
        String toString()
        用于将当前对象转换为一个字符串，很少会主动调用这个方法，多用于输出对象内容使用，
        子类不重写时，默认返回的字符串内容是当前对象的地址信息，实际开发中作用不大
         */
        String str=p.toString();//几乎不会主动调用这个方法
        System.out.println(str);
        //以下情况下的toString会被调用
        //1、输出一个对象到控制台时
        System.out.println(p);//方法内部会调用p.toString将返回的字符串输出到控制台
        //2.任何对象和字符串连接都是字符串（过程中会将其他对象调用toString转换为String连接）
        String line="hello!!!"+p;
        System.out.println(line);

        /*
        Object定义方法
        boolean equals(object o)
        比较当前对象和参数对象o内容是否相同，相同则返回true,否则返回false
        object中该方法的实现内部用“==”（比地址）比较，因此子类补充些该方法则没有实际意义。
         */
        Point p2=new Point(1,2);
        System.out.println("p2:"+p2);
        System.out.println(p==p2);//false
        System.out.println(p.equals(p2));//true，在point有重写，equals(比内容)

    }

}
