package collection;

import sun.plugin.javascript.navig.Link;

import java.util.LinkedList;
import java.util.Queue;
import java.util.jar.JarOutputStream;

/**
 * 队列java.util.Queue
 * 队列Queue接口继承自Collection,因此队列可以保存一组元素，但是存取元素必须
 * 遵循先进先出原则。FIFO:First Input First Output
 * 常见实现类：LinkedList线性表
 */
public class QueueDemo {
    public static void main(String[] args) {
        Queue<String> queue = new LinkedList<>();
        //offer方法是队列提供的入列操作，将元素添加队列到末尾
        queue.offer("one");
        queue.offer("two");
        queue.offer("three");
        queue.offer("four");
        queue.offer("five");
        System.out.println(queue);
        //poll方法是出队操作，获取并删除队首元素
        String poll = queue.poll();
        System.out.println(poll);//one
        System.out.println(queue);//[two, three, four, five]

        //peek方法引用队首元素，获取后元素还在队列中
        poll = queue.peek();
        System.out.println(poll);//two
        System.out.println(queue);//[two, three, four, five]
        //由于队列是一个集合，因此Collection中的操作都支持
        System.out.println(queue.size());//4
        //使用迭代器（新循环），元素仍然在队列中
//        for (String e : queue) {
//            System.out.println(e);
//        }
//        System.out.println(queue);
        //使用poll遍历队列（一次性的）
        //使用poll方法遍历队列（一次性的）
        while (queue.size() > 0) {
            String e = queue.poll();
            System.out.println(e);//two, three, four, five
        }
        System.out.println(queue);//[]
//        for(int i=queue.size();i>0;i--){
//            String e=queue.poll();
//            System.out.println(e);
//        }
//        System.out.println(queue);
//    }
    }
}
