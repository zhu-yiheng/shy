package collection;

import java.util.*;

/**
 * 排序自定义类型元素
 */
public class SortListDemo2 {
    public static void main(String[] args) {
        List<Point> list = new ArrayList<>();
        list.add(new Point(1,2));
        list.add(new Point(8,9));
        list.add(new Point(12,15));
        list.add(new Point(5,4));
        list.add(new Point(3,2));
        System.out.println(list);
        /*
        下面代码编译不通过，因为Collections.sort（List list）
        这个方法要求给定的集合中的元素必须是实现接口：Comparable接口，这个接口是用来
        规定实现类是可以比较大小的。其中要求必须重写方法：
        compareTo用来定义该类实例之间比较大小的规则
        所以只有实现了该接口，sort方法才能利用元素中compareTo方法
        在它们之间比较大小从而进行排序
         */
       // Collections.sort(list);
        /*
        重载sort的方法要求再传入一个参数Comparator
        这是一个接口，用于临时定义一种比较规则，然后在排序的时候将其传入给sort方法，
        这时sort方法就不再要求集合元素必须实现Comparabale接口了，而是利用给定的比较器
        将集合元素比较大小后进行排序。
        由于使用这个方法不再要求修改其他地方的代码，因此这个操作没有侵入性

         */
//                Collections.sort(list, new Comparator<Point>() {
//                    public int compare(Point o1, Point o2) {
//                        int olen1 = o1.getX() * o1.getX() + o1.getY() * o1.getY();
//                        //o1的x坐标与y的坐标的平方
//                        int olen2 = o2.getX() * o2.getX() + o2.getY() * o2.getY();
//                        return olen1 - olen2;
//                    }
//                });

        Collections.sort(list,
                (o1,o2)-> o1.getX() * o1.getX() + o1.getY() * o1.getY()-
                          o2.getX() * o2.getX() - o2.getY() * o2.getY()
        );//此处是lambda表达式

        System.out.println(list);
        //[(1,2), (3,2), (5,4), (8,9), (12,15)]
    }
}
