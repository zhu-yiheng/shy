package collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * JAVA集合
 * 集合与数组一样，可以存放一组元素。但是集合有多种不同的数据结构可供用。将来
 * 我们可以根据不同数据结构特点来保存一组元素并进行使用
 *
 * java.util.Collection是所有集合的顶级接口，里面定义了所有集合都必须具备
 * 的相关功能
 * Collection下面有两个常见的子接口
 * java.util.List：线性表，特点是可以存放重复元素，并有序
 * java.util.Set：不可以存放重复元素
 * 元素是否重复是依靠元素自身equals方法比较结果判定的
 *
 */
public class CollectionDemo {
    public static void main(String[] args) {

        Collection c=new ArrayList();
        /*
        boolean add(E e)
        将给元素添加到集合当中，元素成功添加后返回true
         */
        c.add("one");
        c.add("two");
        c.add("three");
        c.add("four");
        c.add("five");
        System.out.println(c);//[one, two, three, four, five]
        /*
        int size()
        返回当前集合的元素个数
         */
        int size=c.size();
        System.out.println("size:"+size);
        /*
        boolean isEmpty()
        判断当前集合是否为空集(集合size为0时，返回true)
         */
        boolean isEmpty=c.isEmpty();
        System.out.println("是否为空集"+isEmpty);//是否为空集false
        /*
        void clear()
        清空集合
         */
        c.clear();
        System.out.println(c);//
        System.out.println("size:"+c.size());//size:0
        System.out.println("是否为空集"+c.isEmpty());//是否为空集true
    }
}
