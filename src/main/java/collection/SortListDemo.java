package collection;

import java.util.*;

/**
 * 集合的排序
 * 集合的工具类：java.util.Collection提供了静态方法sort，
 * 可以对list集合进行自然排序
 */
public class SortListDemo {
    public static void main(String[] args) {
        List<Integer> list=new ArrayList<>();
        Random random=new Random();
        for(int i=0;i<10;i++){
            list.add(random.nextInt(100));
        }
        System.out.println(list);//[24, 24, 88, 22, 79, 31, 12, 7, 17, 25]
        Collections.sort(list);
        System.out.println(list);//[7, 12, 17, 22, 24, 24, 25, 31, 79, 88]
//      Collections.sort( list,//倒叙
//              (o1,o2)->o2-o1
//      );
        System.out.println(list);
        /*
        偶数再前，奇数再后。
        并且数字从小到大（偶数部分从小和奇数部分）
         */

        Collections.sort(list,
                (o1,o2)->o1%2==o2%2?o1-o2:o1%2-o2%2//三目表达式
        );

        System.out.println(list);

//        Collections.sort(list,new Comparator<Integer>(){
//
//            public int compare(Integer o1, Integer o2) {
//                if(o1%2==o2%2){//判断同奇、同偶
//                    return o1-o2;
//                }else{
//                    return o1%2-o2%2;
//                }
//
//
//            }
//
//
//        });
     //   System.out.println(list);

    }
}
