package collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 数组转换为List集合
 * 数组的工具类Arrays提供了一个静态方法asList,可以将数组转换为一个List集合
 */
public class ArrayToListDemo {
    public static void main(String[] args) {
        String[] array={"one","two","three","four","five"};
        System.out.println(Arrays.toString(array));
        List<String>  list=Arrays.asList(array);
        System.out.println(list);

        list.set(1,"six");
        System.out.println(list);
        /*
        数组跟着改变了，注意：对数组转换的集合进行元素操作就是对原数组，对应的操作
         */
        System.out.println(Arrays.toString(array));
        /*
        由于数组是定长的，因此该集合进行增删元素的操作是不支持的。会抛出
        异常：java.lang.UnsupportedOperationException
         */
//        list.add("seven");
//        System.out.println(list);
//        System.out.println(Arrays.toString(array));
        /*
        若希望对集合增删操作，这需要自行创建一个集合，然后将该集合元素导入。
         */
//        List<String> list2=new ArrayList<>();
//        list2.addAll(list);
        /*
        所有的集合都支持一个参数为Collection的构造方法，
        作用是在创建当前集合的同时包含给定集合中的所有元素
         */
        List<String> list2=new ArrayList<>(list);
        System.out.println("list2:"+list2);
        list2.add("seven");
        System.out.println("list2:"+list2);

    }
}
