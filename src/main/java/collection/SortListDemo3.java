package collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * 排序字符串
 * 当集合元素已经实现了comparable接口，但是该比较规则不满足我们排序需求时，
 * 也可以传入一个比较器
 * 自定义比较规则进行排序
 */
public class SortListDemo3 {
    public static void main(String[] args) {
        List<String> list=new ArrayList<>();
        list.add("范老师");
        list.add("传奇");
        list.add("小泽老师");
        System.out.println(list);
//        Collections.sort(list);
//        Collections.sort(list,new Comparator<String>(){
//            public int compare(String o1,String o2){
//                return  o1.length()-o2.length();
//            }
//        });
//        Collections.sort(list,
//                (o1, o2) -> o1.length()-o2.length()
//        );
        Collections.sort(list,
                (o1, o2) -> o2.length()-o1.length()
        );
        System.out.println(list);//[小泽老师, 范老师, 传奇]
    }
}

