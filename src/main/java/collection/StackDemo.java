package collection;

import java.util.Deque;
import java.util.LinkedList;

/**
 * 栈结构
 * 栈可以保存一组数据元素，但是存取元素必须遵循先进后出原则，
 * FIFO:First Input First Output
 * Deque双端队列如果只调用从同一侧做出入队操作就形成了栈结构。
 * 因此双端队列为栈提供了两个经典的放法名：push(入栈)，pop(出栈)
 * 通常我们使用栈结构用来完成入“后退“这样的功能
 */
public class StackDemo {
    public static void main(String[] args) {
        Deque<String> stack=new LinkedList<>();
        stack.push("one");
        stack.push("two");
        stack.push("three");
        stack.push("four");
        System.out.println(stack);//[four, three, two, one]

        String e=stack.pop();
        System.out.println(e);//four
        System.out.println(stack);//[ three, two, one]
//        for(String e1:stack){
//            System.out.println(e1);
//        }
//        System.out.println(stack);
      while(stack.size()>0){
          String e2=stack.pop();
          System.out.println(e2);//three, two, one
      }
        System.out.println(stack);//[]
    }
}
