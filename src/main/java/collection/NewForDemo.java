package collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * JDK5推出时，推出了一个新特性：增强型for循环
 * 也称为新循环，它可以用相同的语法遍历集合或数组
 *
 * 新循环是java编译器认可的，并非虚拟机
 */
public class NewForDemo {
    public static void main(String[] args) {
        String[] array={"one","two","three","four","five"};
        for(int i=0;i<array.length;i++){
            String str=array[i];
            System.out.println(str);
        }
        /*
        新循环遍历数组就是普通for循环遍历，编译器编译后就会改为普通for循环
         */
        for (String str:array) {
            //增强版 String str表示的是定义的变量来接受数组每个元素
            System.out.println(str);
        }
        /*
        泛型就JDK5之后推出的另一个特性
        泛型也称为参数化类型，允许我们在使用一个类时指定他里面的属性的类型
        方法参数或返回值的类型，使得我们在使用一类时可以更灵活
        泛型被广发应用于集合中，用来指定集合中的元素类型
        支持泛型的类在使用时如果未指定泛型，那么就默认在原类型的Object
        Collection接口的定义

        Collection中的add方法的定义，参数为E
        boolean add(E e)
         */
        Collection<String> c=new ArrayList<>();
        c.add("one");//编译器会检查add方法的实参是否为String 类型
        c.add("two");
        c.add("three");
        c.add("four");
        c.add("five");
        //编译器不通过    c.add("123");
        //迭代器遍历
        //迭代器也支持泛型，指定的与其遍历的集合指定的泛型一致即可
        Iterator<String>it =c.iterator();

        while (it.hasNext()){
            //编译器编译代码时根据迭代器指定的泛型补充造型代码
            String str=(String) it.next();//获取元素时无需在造型
            System.out.println(str);
        }
        /*
        新循环遍历集合就是迭代器遍历，编译器编译代码后会改为迭代器
        因此不要在新循环遍历集合的过程中使用集合的方法增删元素。
         */
        //新循环遍历
        for (String str:c){
            System.out.println(str);
        }
    }
}
