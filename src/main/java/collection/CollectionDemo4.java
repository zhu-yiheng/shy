package collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

/**
 *集合间的操作
 */
public class CollectionDemo4 {
    public static void main(String[] args) {

        // Collection c1=new ArrayList();
        Collection c1=new HashSet();
        c1.add("java");
        c1.add("c++");
        c1.add("c");

        System.out.println("c1"+c1);
        Collection c2=new ArrayList();
        c2.add("and");
        c2.add("c");
        System.out.println("c2"+c2);


        c1.addAll(c2);
        /*
        boolean addAll(Collection c)
        将给定集合中的所有元素添加到当前集合中。
        当前集合中若发生了改变则返回true
       addAll取的是并集
       c1.addAll(c2);的意思是c1与c2相并
         */
        System.out.println("c1"+c1);//c1[java, c++, c, and,pty]
        //c1[c++, java, c, and],使用hashSet是重复元素不包括进去
        System.out.println("c2"+c2);//c2[and, pty]//c2[and, c]

        Collection c3=new ArrayList();
        c3.add("ios");
        c3.add("c++");
        System.out.println("c3"+c3);
        /*
        boolean containsAll(Collection c)
        判断当前集合是否包含给定集合的所有元素
         */
        boolean contains=c1.containsAll(c3);
        System.out.println("包含所有元素"+contains);
        /*
        boolean removeAll(Collection c)
        删除当前集合中与给定集合中的共有元素
        之删除一个集合中相同的元素
         */
        c1.removeAll(c3);
        System.out.println("c1"+c1);//c1[java, c, and]
        System.out.println("c3"+c3);//c3[ios, c++]
    }
}
