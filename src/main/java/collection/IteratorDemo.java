package collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Collection接口没有定义贩毒获取某个元素的操作，因为不通用
 * 但是Collection提供了遍历集合元素的操作。该操作是一个通用的操作，
 * 无论什么类型的集合都支持此种遍历方式：迭代器模式
 *
 * Iterator iterator（）
 * 该方法会获取一个用于遍历当前集合元素的迭代器
 *
 * java.util.Iterator接口，是迭代器的接口，规定了迭代器遍历集合的相关操作，
 * 不同的集合都提供了一个用于遍历自身元素的迭代器实现类，不过我们不需要知道他们的名字
 * 以多态的方式当成 Iterator使用即可
 * 迭代器遍历集合遵循的步骤：wen->取->删
 * 其中删除不是必须操作
 */
public class IteratorDemo {
    public static void main(String[] args) {
        Collection c=new ArrayList();
        c.add("one");
        c.add("#");
        c.add("two");
        c.add("#");
        c.add("three");
        c.add("#");
        c.add("four");
        c.add("#");
        c.add("five");
        System.out.println(c);//[one, two, three, four, five]
        Iterator it=c.iterator();
        /*
        迭代器提供的相关方法
        boolean hasNext()
        判断集合是否还有元素可以遍历

        E next()
        获取下一个元素（第一次调用时就是获取第一个元素，一次类推）
         */

        while (it.hasNext()) {
            String str=(String) it.next();
            System.out.println(str);//one two three  four five


            if ("#".equals(str)) {
                /*
            迭代器要求遍历的过程中不得通过集合的方法增删元素
            否则会抛出异常 ConcurrentModificationException
             */
              //  c.remove(str);
                /*
                迭代器的remove方法可以将通过next方法获取的元素冲击和中删除。
                 */
                it.remove();
            }
        }
        System.out.println(c);
    }
}
