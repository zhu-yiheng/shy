package collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 *List集合
 * List是Collection下面常见的一类集合
 * java.util.List接口是所有List的接口，它继承自Collection.
 * 常见的实现类：
 * java.util.ArrayList:内部有数组实现，查询性能更好
 * java.util.LinkedList:内部由链表实现，增删性能好
 * List集合的特点是：可以存放重复元素，并且有序。并且有序，】
 * 其提供了一套可以通过下标提供元素
 */
public class ListDemo {
    public static void main(String[] args) {
        List<String> list= new ArrayList<>();
        //
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        list.add("five");
        /*
        E get(int index)
        获取下标对应的元素
         */
        String e=list.get(2);
        System.out.println(e);//three

        for(int i=0;i<list.size();i++){
            e=list.get(i);
            System.out.println(e);//one two three four five
        }
       /*
       E set()
       将给定的元素设置到指定位置，返回值为该位置原有的元素。
       替换元素操作
        */
        //[one,six,three,four,five]
        String old=list.set(1,"six");
        System.out.println(list);
        System.out.println("被替换的元素是："+old);//被替换的元素是：two

        //在不创建新集合的前提下，将list集合元素翻转
//        for(int i=0;i<list.size()/2;i++){
//            e=list.get(list.size()-1-i);//获取倒数位置的元素
//            e=list.set(i,e);//将倒数位置元素设置到整数替换元素
//            list.set(list.size()-1-i,e);//再将整数位置元素设置到倒数位置上
//        }

        //[five,four,three,six,one]
        Collections.reverse(list);//反转
        System.out.println(list);
    }
}

