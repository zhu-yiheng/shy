package collection;

import java.util.ArrayList;
import java.util.List;

/**
 * List subList(int start, int end)
 * 获取当前集合中指定范围内的子集，两个参数为开始与结束的下标（含头不含尾）
 */
public class ListDemo3 {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(i);
        }

        System.out.println(list);
        //获取3-7这部分
        List<Integer> subList = list.subList(3, 8);
        System.out.println(subList);
        //将子集每个元素扩大10倍

        for (int i = 0; i < subList.size(); i++){

          subList.set(i,subList.get(i)*10);
    }
        //30,40,50,60,70
        System.out.println(subList);
        /*
        对子集元素的操作就是对原集合对应元素的操作
         */
        System.out.println(list);//[0, 1, 2, 30, 40, 50, 60, 70, 8, 9]
        //删除list集合中的2-8
        list.subList(2,9).clear();
        System.out.println(list);
    }
}
