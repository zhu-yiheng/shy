package collection;

import java.util.Deque;
import java.util.LinkedList;

/**
 * 双端队列
 * java.util.Deque接口，双端队列是队列两端都可以做出入列的队列
 * Deque继承自Queue,常用是实现类：LinkedList
 */
public class DequeDemo {
    public static void main(String[] args) {
        Deque<String> deque=new LinkedList<>();
        deque.offer("one");
        deque.offer("two");
        deque.offer("three");
        System.out.println(deque);//[one, two, three]
        //队首方向入列
        deque.offerFirst("four");
        System.out.println(deque);//[four, one, two, three]
        //队尾方向入列，与offer一致
        deque.offerLast("five");
        System.out.println(deque);//[four, one, two, three, five]

        String e=deque.poll();
        System.out.println(e);//four
        System.out.println(deque);//[one, two, three, five]
        //队首方向出列
        e=deque.pollFirst();
        System.out.println(e);//one
        System.out.println(deque);//[two, three, five]
        //队尾方向出列
        e=deque.pollLast();
        System.out.println(e);//five
        System.out.println(deque);//[two, three]
    }
}
