package collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

/**
 * 集合的有很多操作有与元素的equals方法相关
 *HashSet()相当于数组只放进去了一次，删除重复项时，就不会再出现此项
 * ArrayList() 在删除重复项时，重复的删除
 */
public class CollectionDemo2 {
    public static void main(String[] args) {
        //Collection c=new ArrayList();
        Collection c=new HashSet();
        c.add(new Point(1,2));
        c.add(new Point(3,4));
        c.add(new Point(5,6));
        c.add(new Point(7,8));
        c.add(new Point(1,2));
        /*
        集合重写了Object的toString方法，输出的格式为：
        [元素1.toString(), 元素2：toString(),......]
         */
        System.out.println(c);

        Point p=new Point(1,2);
        /*
        boolean contains(Object o)
        判断当前集合是否包含给定元素，这里判断的依据是给顶元素是否与集合
        现有元素存在equals比较为true的情况。
         */
        boolean contains=c.contains(p);
        System.out.println("包含："+contains);

    /*
    remove用来从集合中删除给定的元素，删除的也是与集合中equals比较
    为true的元素。注意，对于可以存放重复元素的集合而言，只删一次
     */
  c.remove(p);
  System.out.println(c);
}
}
