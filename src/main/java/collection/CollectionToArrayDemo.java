package collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 集合转换为数组
 * Collection提供了方法toArray可以将当前集合转换为一个数组
 */
public class CollectionToArrayDemo {
    public static void main(String[] args) {
        List<String> list= new ArrayList<>();
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        list.add("five");
        System.out.println(list);
      // Object[] array= list.toArray();
        /*
        重载的toArray方法要求传入一个数组，内部会将集合所有元素存入该数组后将其
        返回（前提是该数组长度>=集合的size）。如果给定的数组长度不足
        则方法内部会自行根据给数组类型创建一个与集合size
        一致长度的数组并将集合元素存入后返回
         */
       String[] array= list.toArray(new String[list.size()]);
        System.out.println(array.length);
        System.out.println(Arrays.toString(array));
    }
}
