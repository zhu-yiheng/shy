package socket;

import java.util.Arrays;

public class Test {
    public static void main(String[] args) {
        int[] allOut={1,4,5,87,9,342,76,21,45,123};
        int pw=21;
        System.out.println(Arrays.toString(allOut));
        //将pw从allOut数组中删除
        //把指定元素替换为最后一个元素
        for(int i=0;i<allOut.length;i++){
            if(allOut[i]==pw){
                allOut[i]=allOut[allOut.length-1];
                allOut=Arrays.copyOf(allOut,allOut.length-1);
                break;
            }
        }

        System.out.println(Arrays.toString(allOut));
    }
}
