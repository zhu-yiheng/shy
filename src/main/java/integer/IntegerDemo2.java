package integer;

/**
 * 包装类常用的功能
 */

public class IntegerDemo2 {
    public static void main(String[] args) {
        //1通过包装类可以获取其对应基本类型的取值范围
        //获取int的最大值和最小值
        //Short,Byte,Integer,Long,Float,Double,Boolean,Character
        int imax=Integer.MAX_VALUE;
        System.out.println(imax);
        int imin=Integer.MIN_VALUE;
        System.out.println(imin);

        long lmax=Integer.MAX_VALUE;
        long lmin=Integer.MIN_VALUE;
        System.out.println(lmax);
        System.out.println(lmin);

        double dmax=Integer.MAX_VALUE;
        double dmin=Integer.MIN_VALUE;
        System.out.println(dmax);
        System.out.println(dmin);

        //把字符串解析为对应的基本类型
        /*
        2:包装类可以把字符串解析为对应的基本类型
       注意:该字符串必须正确描述了基本类型可以保存的值，否则转换时会抛出
            异常:NumberFormatException
         */
        String a="123";
       // String a="123.123";
        int s=Integer.parseInt(a);
        System.out.println(s);
        double d=Double.parseDouble(a);
        System.out.println(d);//123.0


    }
}
