package integer;

/**
 * 包装类
 * 包装类是为了解决基本类型不能直接参与面向对象开发（没有面向对象三大特性），因此
 * 包装类可以让基本类型以“对象”形式存在
 * */

public class IntegerDemo1 {
    public static void main(String[] args) {
        int i=123;
        //java 不推荐使用new创建包装类
        //java推荐我们使用包装类的静态方法valueOf将基本类型转换为包装类
        // 而不是直接new
        Integer  i1=new Integer(i);
        Integer i2=new Integer(i);
        System.out.println(i1==i2);//false
        System.out.println(i1.equals(i2));//true
        //重用1字节之内的正数对象（-128-127）
        Integer i3=Integer.valueOf(i);
        Integer i4=Integer.valueOf(i);
        System.out.println(i3);
        System.out.println(i3==i4);//true
        System.out.println(i3.equals(i4));//true
        //double的valueof内部就是直接new对象
        Double d1=Double.valueOf(123.123);
        Double d2=Double.valueOf(123.123);
        System.out.println(d1==d2);//false
        System.out.println(d1.equals(d2));//true
        //包装类转回基本类型
        int ii=i1.intValue();//123
        Double dd=i1.doubleValue();//123.0
        System.out.println(ii);
        System.out.println(dd);

        ii=d1.intValue();//123大类型转小类型会丢失精度
        dd=d1.doubleValue();//123.123
        System.out.println(ii);
        System.out.println(dd);
    }
}
