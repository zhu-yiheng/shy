package integer;

/**
 * JDK1.5之后，Java推出了一个新的特性：自动拆装箱
 * 该特性是编译器认可的，当编译器编译源代码时发现有基本类型和引用类型互相赋值操作
 * 时会补充代码将基本类型与对应的包装类互相转换
 */

public class IntegeDemo3 {
    public static void main(String[] args) {
        int i=123;
        /*
        触发了编译器的自动装箱的特性，代码会被编译器改为
        Integer in=Integer.valueOf(i)
         */
        Integer in=i;
        /*
        触动了自动拆箱的特性，代码会被编译器改为
        i=in.intValue();
         */
        i=in;
    }
}
