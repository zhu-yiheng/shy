package api;

/**
 * 文档注释是功能级注释，用来说明一个类，一个方法或一个量，因此只在以上方法中使用
 * 文档注释可以使用java中自带的命令Javadoc来对这个类生成手册
 *
 * 在类上使用时来说明当前类的整体功能
 * @author  Daisy
 */
public class Demo {
    /**
     * sayHllo中使用的问候语
     */
    public static final String INFO="Hello!";

    /**
     * 为给定的客户添加用户语
     * @param name  指定的用户名字
     * @return  返回了含有问候语的字符串
     */
    public String sayHello(String name){
        return "Hello"+name;

    }

}
