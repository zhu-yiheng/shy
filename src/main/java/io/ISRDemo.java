package io;

import java.io.*;

/**
 * 使用转换输入流读取文本数据
 */
public class ISRDemo {
    public static void main(String[] args) throws IOException {
        //将ows.txt的文件中的读取出来
        FileInputStream fis=new FileInputStream("ows.txt");
        InputStreamReader isr
                = new InputStreamReader(fis,"UTF-8");
        /*
        int read()
        一次读取一个字符，返回值的int值“低16位”有效，因为插入占2字节
        但是如果返回的int值为-1，则表示读取到了文件末尾

         */
        int d;
        while((d=isr.read())!=-1){
            System.out.println((char) d);


        }
        isr.close();
    }
    }

