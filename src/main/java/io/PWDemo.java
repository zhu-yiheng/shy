package io;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * 缓冲字符流
 * java.io.BufferedWriter和BufferedReader
 * 缓冲字符输出与输入流内部都有一个缓冲区，可以块读写文本数据加快读写效率，并且可以按行读写字符串
 *
 *
 *
 * java.io.PrintWriter是具有自动行刷新的缓冲字符输出流，内部总是自动连接
 * BufferedWriter作为缓冲加速功能。实际开发中PrintWriter更常用
 */

public class PWDemo {
    public static void main(String[] args) throws FileNotFoundException {
        //向文件pw.txt写入文本数据
        /*
        PrintWriter提供了直接对文件做写操作的构造方法
         PrintWriter（File file）
          PrintWriter(String path)
          还支持第二个参数，用来指定字符集
         */
        PrintWriter pw=new PrintWriter("pw.txt");
        pw.println("我可以包容你的所有小脾气");
        pw.println("我可以带你你去吃好多好吃的");
        System.out.println("完毕");
        pw.close();
    }
}
