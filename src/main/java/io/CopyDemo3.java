package io;

import java.io.*;

/**
 * 使用缓冲流完成文件的读写
 *
 * 缓冲流：java.io.BufferedInputStream和BufferedOutputStream
 * 它们是一对高级流，在流连接的作用是加快读写效率。
 * 缓冲流内部维护一个缓冲区，默认长度为8k。缓冲流最终会将读写数据统一转换为
 * 块读写来保证读写效率。
 */
public class CopyDemo3 {
    public static void main(String[] args) throws IOException {
        FileInputStream fis=new FileInputStream("wnwb.exe");
        BufferedInputStream bis=new BufferedInputStream(fis);

        FileOutputStream fos=new FileOutputStream("wnwb_cp.exe");
        BufferedOutputStream bos=new BufferedOutputStream(fos);

        int d;
        while((d=bis.read())!=-1){
            bos.write(d);
        }
        System.out.println("复制完毕");
        bis.close();
        bos.close();

    }
}

