package io;

import java.io.*;

/**
 * JAVA IO将按照读写单位划分为字节流与字符流
 * 字节流的超类：InputStream和OutputStream
 * 字符流的超类：Reader和Writer
 * 字符流是以字符（char）为最小单位读写数据的，底层本质还是读写字节的，只不过字符与字节的转换工作有字符流完成
 *
 * 转换流 ：InputStreamReader和OutputStreamWriter
 * 转换流是一种非常常用的一对字符流实现类，虽然实际开发中我们几乎不会直接操作它们
 * 但是在流连接中它们是非常重要的一环
 *
 * 由于实际开发中其他的高级字符流在流连接中连接在其他的字符流上，而低级流都是字节流，
 * 这就导致了无法直接进行串联。转换流本身就是字节流，它们又可以直接连接在直接流上，
 * 因此其他字符流链接在它上就可以达到串联字节流的目的了，它相当于是一个
 * “转换器”的作用
 */

public class OSWDemo {
    public static void main(String[] args) throws IOException {
        //向文件osw.txt中写入文本数据
        FileOutputStream fos=new FileOutputStream("ows.txt");
        /*
        转换流在进行流连接时通常会指定第二个参数用来明确字符集，
        这样通过它写出的字符都会按照该字符集转换为字节写出
         */
        OutputStreamWriter osw
                =new OutputStreamWriter(fos,"UTF-8");

        //字符流可以直接写出字符串，无需再关心字符串转换文字的操作
        osw.write("让我再看你一眼");
        osw.write("feraf");
        System.out.println("完毕");
        osw.close();

    }
}
