package io;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * 使用文件流向文件中写入文本数据
 */

public class WritStringDemo {
    public static void main(String[] args) throws IOException {
        //向文件fos.txt中写入文本数据
        FileOutputStream fos=new FileOutputStream("fos.txt");
        String line="万张楼高平地起，辉煌只能靠自己!";
        /*
        String 提供了将字符串转换为一组字节的方法
        byte[] fetBytes(String charsetName)
        参数为字符集的名字，常用的是UTF-8.
        其中中文字3字节表示1个，英文1个字节表示1个
        UTF-8称为万国码，互联网中最常用的字符集，是Unicode的变长编码
        GBK是我国的国际编码，英文占一字节，中文占2字节
         */
        byte[] data=line.getBytes("utf-8");
        fos.write(data);
        line="社会很单纯，复杂的是人";
        data=line.getBytes("UTF-8");
        fos.write(data);
        System.out.println("输出完毕");
        fos.close();

    }
}
