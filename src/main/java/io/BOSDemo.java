package io;

import java.io.*;

/**
 * 缓冲流输出流写数据的时效性问题（缓冲区问题）
 */
public class BOSDemo {
    public static void main(String[] args) throws IOException {
        FileOutputStream fos=new FileOutputStream("bos.txt");
        BufferedOutputStream bos=new BufferedOutputStream(fos);
        String line="画画的babay";
        byte[] data=line.getBytes("UTF-8");
        bos.write(data);
        /*
        void flush()
        将缓冲流的缓冲区中已存的数据一次性写出
        注：频繁调用该方法会提高写出的次数降低写出效率。但是可以换来写出数据的及时性
        flush的方法被定义在了输出的流的超类上了，这意味着所有的输出流都有flush方法，目的是
        在流连接中传递flush操作给缓冲输出流
        其他高级流的flush只是调用了它的链接流中的flush
         */
        bos.flush();
        System.out.println("写出完毕");
        /*
       缓冲流的close操纵会自动调用一次flush，确保所有缓出的数据流出
         */
        bos.close();
    }
}


