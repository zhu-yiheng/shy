package io;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 重载的构造方法可以将文件输出流创建为追加模式
 * FileOutStream(String path,boolean append)
 * FileOutStream(File file,boolean append)
 * 当第二参数传入true时，文件流为追加模式，：即：指定的文件若存在，则原有数据保留
 * 新写入的数据会被顺序追加到文件中
 */
public class FOSDemo2 {
    public static void main(String[] args) throws IOException {
        FileOutputStream fos
                = new FileOutputStream("./fos.txt",true);
        fos.write(97);

        /*
        97的二进制
        00000000 00000000 00000000 01100001
        文件数据
        01100001
         */
        fos.write(98);
         /*
        98的二进制
        00000000 00000000 00000000 01100010
        文件数据
        01100010
         */
        fos.write(99);
         /*
        99的二进制
        00000000 00000000 00000000 01100011
        文件数据
        01100001 01100010 01100011
         */
        System.out.println("写出完毕");
        fos.close();
    }
}
