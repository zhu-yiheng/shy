package io;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

import static javafx.application.Platform.exit;

/**
 * 简易记事本工具
 * 程序启动后要求输入一个文件名，然后对该文件写操作
 * 之后用户输入的每行字符串都写入到文件中（写入到文件中的数据不考虑换行）
 * 当单独输入exit时程序退出。
 */
public class NoteDemo {
    public static void main(String[] args) throws IOException {

        Scanner scan = new Scanner(System.in);
        System.out.println("请输入文件名");
        String name = scan.nextLine();
        FileOutputStream fos = new FileOutputStream(name);//根据用户输入的文件来找文件
        System.out.println("请开始输入内容，单独输入exit时退出");
        while (true) {
            String line = scan.nextLine();//输入的内容
            if ("exit".equals(line)) {
                break;
            }
                byte[] data       = line.getBytes("utf-8");
                fos.write(data);
            }
        System.out.println("再见！");
        fos.close();

        }
    }

