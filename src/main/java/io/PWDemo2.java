package io;

import java.io.*;
import java.util.Scanner;

/**
 * 在流连接中使用PrintWriter
 */
public class PWDemo2 {
    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in);
        System.out.println("请输入文件名");
        String fileName = scan.nextLine();

        //低级流用于向文件中写入字节
        //FileOutputStream fos=new FileOutputStream("pw.txt");
        FileOutputStream fos = new FileOutputStream(fileName);
        //高级流，1：衔接字节与字符流  2：将写出的字符转换为字节
        OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
        //高级流，块写文本数据加速
        BufferedWriter bw = new BufferedWriter(osw);
        /*
        PrintWriter构造方法中如果是第一个参数是一个流，那么就支持一个boolean型的参数，
        表示表示自动行刷新。当这个参数为true时，就打开了自动行刷新，此时每通过PrintWriter的println方法写出一行
        字符串后就会自动flush一次，注意：print方法不会自动刷新！！！

         */
        //高级流,自动刷新，按行写出字符串
        PrintWriter pw = new PrintWriter(bw,true);
        System.out.println("请开始输入内容,单独输入exit时退出");
        String line;
        while (!"exit".equals(line=scan.nextLine())) {
           pw.println(line);
           pw.flush();
        }
        System.out.println("完毕");
        pw.close();
    }
}