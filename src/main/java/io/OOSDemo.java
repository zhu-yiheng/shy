package io;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * 对象流
 * java.io.objectOutputStream和ObjectInputStream
 * 对象流是一种高级流，用于进行对象的序列化与反序列化
 *
 */

public class OOSDemo {
    public static void main(String[] args) throws IOException {
        //将一个Person对象写入文件person.obj
        FileOutputStream fos=new FileOutputStream("person.obj");
        ObjectOutputStream oos=new ObjectOutputStream(fos);
        String name="canglaoshi";
        int age=18;
        String gender="nv";
        String[] otherInfo={"是一名yanyuan","来自、岛国"};
        Person p=new Person(name,age,gender,otherInfo);
        System.out.println(p);
        /*
        void writeObject(Object obj)
        对象流提供的方法可以进行对象序列化。
        该方法要求必须实现可序列化接口（java.io.Serializable）,否则会报异常
        java.io.NotSerializableException: io.Person

 */
        oos.writeObject(p);
        System.out.println("完毕");
        oos.close();
    }
}
