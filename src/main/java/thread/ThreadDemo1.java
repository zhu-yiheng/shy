package thread;

/**
 * 多线程
 * 线程：程序中一个单一的执行流程
 * 多线程：多个单一顺序执行流程“同时”执行
 *
 * 线程之间是并发执行的，并非真正意义上的同时
 * 常见线程有两种方式：
 * 1：继承thread并重写run方法
 *
 */
public class ThreadDemo1 {
    public static void main(String[] args) {
     //创建两个线程
        Thread t1=new MyTread1();
        Thread t2=new MyThead2();
        /*
        启动线程注意不要调用run方法
        线程调用完start方法会纳入到系统的线程调度器程序中被统一管理
        线程调度器会分配时间片段给线程，使得CPU执行该线程这段时间，
        用完后线程调度后会再分配一个时间片段给线程，如此反复，使得多个线程
        都有机会执行一次，做到走走停停并发运行
        线程第一次被分配到时间后会执行它的run方法开始工作
         */

        t1.start();
        t2.start();

    }
}

/**
 * 第一种创建线程的优点
 * 结构简单，利于匿名内部类形式创建
 *
 *
 * 缺点：
 * 1：由于java是单继承的，这会导致继承了Thread就无法再继承其他类区复用方法
 * 2：定义线程的同时重写了run方法，这等于将线程的任务定义在了这个线程中导致
 * 线程只能干这件事。重（chong）用性很低
 */
class MyTread1 extends Thread{
    public void run(){
       for (int i=0;i<1000;i++){
           System.out.println("hello");

       }
    }
}
class MyThead2 extends Thread{
    public void run(){
        for (int i=0;i<1000;i++){
            System.out.println("hello11");

        }
    }
}
