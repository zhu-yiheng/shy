package thread;

/**
 * 线程提供了jion方法，可以协调线程的同步运行，它允许调用该方法的线程
 * 等待（阻塞），直到该方法所属线程执行完毕后等待（阻塞）继续运行
 * 同步运行：多个线程执行存在先后顺序
 * 异步运行：多个线程各干各的，线程间运行本来就是异步的。
 */
public class JoinDemo {
    /*
    当一个方法的局部内部类中引用了这个方法的其他局部变量时，
    这个变量必须是final的
     */
    public static boolean isFinish=false;
    public static void main(String[] args) {
        Thread download= new Thread(){
            public void run(){
                for (int i=0;i<=100;i++) {
                    System.out.println("down:"+i+"%");
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                    }
                }
                System.out.println("down:下载完毕");

            }
        };
        Thread show=new Thread(){
            public void run(){

                try {
                    System.out.println("show:开始显示文字,,,");
                    Thread.sleep(3000);
                    System.out.println("show:显示文字完毕");
                    /*
                    显示图片前要等待download执行完毕
                    此处不能使用sleep，因为无法估算阻塞的时间长短
                     */
                    System.out.println("show:开始等待download...");
                    download.join();
                    System.out.println("show:等待download完毕");

                    System.out.println("show:开始显示图片");
                    if(!isFinish){
                        throw new RuntimeException("show:显示图片失败");
                    }
                    System.out.println("show:显示图片完毕！");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        download.start();
        show.start();
    }
}
