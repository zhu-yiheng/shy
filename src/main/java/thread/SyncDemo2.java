package thread;

/**
 * 有效的缩小同步范围可以在保证并发安全的前提下尽可能的提高并发效率
 * 同步块
 *
 * 语法：
 * synchronized(同步监视器对象){
 *     需要多个线程同步执行的代码片段
 * }
 * 同步块可以更准确的锁定需要多个线程同步自行的代码片段来有效的缩小排队范围
 */
public class SyncDemo2 {
    public static void main(String[] args) {
        Shop shop = new Shop();
        Thread t1 = new Thread() {
            public void run() {
                shop.buy();
            }
        };
        Thread t2 = new Thread() {
            public void run() {
                shop.buy();
            }
        };
        t1.start();
        t2.start();
    }
}
class Shop {
    public void buy(){
        /*
        在方法上使用synchronized，那么同步监视器对象就是this
         */
    //public synchronized  void buy() {
        Thread t = Thread.currentThread();//获取运行该线程的方法
        try {
            System.out.println(t.getName() + ":正在挑衣服");
            Thread.sleep(5000);
            /*
            使用同步块需要制定监视器对象，即：上锁的对象
            这个对象可以是java中任何引用类型的实例，只要保证多个需要排队
            执行该同步块中代码的线程看到的该对象是“同一个”即可
             */
            synchronized (this) {//不能一起干，排队
              //synchronized (new Object()){没有效果
                System.out.println(t.getName() + ":正在试衣服");
                Thread.sleep(5000);
            }

            System.out.println(t.getName() + ":结账离开");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

