package file;


import java.io.File;

/**
 * java.io.File
 * File的每一个实例用于表示硬盘（文件系统）上的一个文件或目录
 * 使用File可以：
 * 1：访问其表示的文件和目录的属性
 * 2：操作文件和目录（创建或删除）
 * 3：访问一个目录中的所有子项
 * 但是不能访问文件数据
 */

public class FileDemo {
    public static void main(String[] args) {
        //使用File访问当前项目目录下的demo.txt文件
        /*
        创建File时，要指定路径，而路径通常使用相对路径
        相对路径的好处，有良好的的跨平台性
        “./”是相对路径中用的最多的，表示“当前目录”，而当前目录是哪里，取决于
        程序运行环境而定，在idea中运行Java程序时，
        这里指的是，当前目录就是在当前程序所在的项目目录
         */

        File file=new File("./demo.txt");
        String name=file.getName();
        System.out.println(name);
        //获取文件大小(单位是字节)
        long len=file.length();
        System.out.println(len+"字节");
        //通过可读或可写，用“canRead/canWrite"


        boolean cr=file.canRead();
        boolean cw=file.canWrite();
        System.out.println("是否可读"+cr);
        System.out.println("是否可写"+cw);
        //通过隐藏，用“isHidden"
        boolean hi=file.isHidden();
        System.out.println("是否隐藏"+hi);
    }

}
