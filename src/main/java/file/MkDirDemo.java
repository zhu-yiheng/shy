package file;

import java.io.File;
import java.util.FormatFlagsConversionMismatchException;

/**
 * 使用File创建目录
 */

public class MkDirDemo {
    public static void main(String[] args) {
        //在当前目录下新建一个Demo
        //File dir=new File("demo");
        File dir=new File("a/b/c/d/e/f");
        if(dir.exists()){
            System.out.println("该目录已存在");
        } else{
            dir.mkdirs();
            System.out.println("目录已创建");
        }
    }
}
