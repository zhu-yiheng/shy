package file;

import java.io.File;
import java.io.IOException;

/**
 * 使用File创建一个新文件
 */
public class CreateNewDemo {
    public static void main(String[] args) throws IOException {
       // File file=new File("./test.txt");
        File file =new File("./");//当前面目录



        //在当前目录下新建一个文件test.txt,file保存文件路径，文件不存在，也可以
        /*
        boolean exists()
        判断当前File,表示的是路径所在位置是否真实存在该文件或目录。存在则返回true
         */
        if(file.exists()){
            System.out.println("改文件已存在");
        } else {
            file.createNewFile();//将File文件创建出来
            System.out.println("该文件已存在");

        }
    }

}
