package file;

import java.io.File;

/**
 * 删除一个目录
 */
public class DelateDirDemo {
    public static void main(String[] args) {
        //将当前目录下的demo目录删除
      //  File dir=new File("demo");
        File dir=new File("a");
        if(dir.exists()){
            dir.delete();//delete只能删除空目录
            System.out.println("已删除");
        } else{
            System.out.println("文件不存在");
        }
    }
}
