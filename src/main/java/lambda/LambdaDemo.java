package lambda;

import java.io.File;
import java.io.FileFilter;

/**
 * JDK8之后Java支持了lambda表达式这个特性
 * lambda表达是可以用更精简的语法创建匿名内部类，但是实现的接口只能有一个抽象
 * 方法，否则无法使用
 * lambda表达式是编译器认可的，最终会被改为内部类形式编译到class文件中
 *
 *
 * 语法：
 * (参数列表)->{
 *     方法体
 *
 * }
 */
public class LambdaDemo {
    public static void main(String[] args) {
        //匿名内部类形式创建FileFilter
        //lambda表达式中参数的类型可以忽略不写
      //FileFilter filter2=(File file)-> {//也可以只写参数名file
          //return file.getName().startsWith(".");
     // };
        //lambda表达式方法体中若只写一句代码，则{}可以省略
        //如果这句话有return关键字，那么return也要一并省略！
       // FileFilter filter2=(file)->file.getName().startsWith(".");
        //使用lambda表达式创建文件过滤器，获取当前目录下所有名字中含有a的子项
        File dir=new File(".");//当前文件
        if(dir.isDirectory()) {
            //File[] subs=dir.listFiles((file) -> file.getName().indexOf("a") >= 0);
            File[] subs=dir.listFiles((file) -> file.getName().contains("a"));
            for(int i=0;i<subs.length;i++){
                System.out.println(subs[i].getName());
            }
        }
    }

}
